<?php
class IndraDesign_ImageGallery_Block_Adminhtml_Gallery_Edit extends Mage_Adminhtml_Block_Widget_Form_Container
{
    protected function _construct()
    {
        $this->_blockGroup = 'indradesign_imagegallery_adminhtml';
        $this->_controller = 'gallery';

        /**
         * The $_mode property tells Magento which folder to use
         * to locate the related form blocks to be displayed in
         * this form container. In our example, this corresponds
         * to ImageGallery/Block/Adminhtml/Gallery/Edit/.
         */
        $this->_mode = 'edit';

        $newOrEdit = $this->getRequest()->getParam('id')?$this->__('Edit'):$this->__('New');
        $this->_headerText =  $newOrEdit . ' ' . $this->__('Gallery Image');
    }
}