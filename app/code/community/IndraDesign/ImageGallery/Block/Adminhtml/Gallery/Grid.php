<?php
class IndraDesign_ImageGallery_Block_Adminhtml_Gallery_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
    protected function _prepareCollection()
    {
        /**
         * Tell Magento which collection to use to display in the grid.
         */
        $collection = Mage::getResourceModel('indradesign_imagegallery/gallery_collection');
        $this->setCollection($collection);
        return parent::_prepareCollection();
    }

    public function getRowUrl($row)
    {
        /**
         * When a grid row is clicked, this is where the user should
         * be redirected to - in our example, the method editAction of
         * GalleryController.php in ImageGallery module.
         */
        return $this->getUrl('indradesign_imagegallery_admin/gallery/edit', array('id' => $row->getId()));
    }

    protected function _prepareColumns()
    {
        /**
         * Here, we'll define which columns to display in the grid.
         */
        $this->addColumn('entity_id', array(
            'header' => $this->_getHelper()->__('ID'),
            'type' => 'number',
            'index' => 'entity_id',
        ));

        $this->addColumn('url_key', array(
            'header' => $this->_getHelper()->__('Url Key'),
            'type' => 'text',
            'index' => 'url_key',
        ));
        
        $this->addColumn('description', array(
            'header' => $this->_getHelper()->__('Description'),
            'type' => 'text',
            'index' => 'description',
        ));
        $this->addColumn('location',array(
	        'header'=> $this->_getHelper()->__('Thumbnail'),
        	'index' => 'location',
	        'frame_callback' => array($this, 'callback_image')
		));

        /**
         * Finally, we'll add an action column with an edit link.
         */
        $this->addColumn('action', array(
            'header' => $this->_getHelper()->__('Action'),
            'width' => '50px',
            'type' => 'action',
            'actions' => array(
                array(
                    'caption' => $this->_getHelper()->__('Edit'),
                    'url' => array(
                        'base' => 'indradesign_imagegallery_admin'
                                  . '/gallery/edit',
                    ),
                    'field' => 'id'
                ),
            ),
            'filter' => false,
            'sortable' => false,
            'index' => 'entity_id',
        ));

        return parent::_prepareColumns();
    }

    protected function _getHelper()
    {
        return Mage::helper('indradesign_imagegallery');
    }
    
    public function callback_image($value)
	{
	    $width = 70;
	    $height = 70;
	    return "<img src='".Mage::getBaseUrl('media').'dhl/'.$value."' width=".$width." height=".$height."/>";
	}

}