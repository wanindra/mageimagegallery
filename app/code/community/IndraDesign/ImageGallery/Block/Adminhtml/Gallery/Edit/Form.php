<?php
class IndraDesign_ImageGallery_Block_Adminhtml_Gallery_Edit_Form extends Mage_Adminhtml_Block_Widget_Form
{
    protected function _prepareForm()
    {
        // Instantiate a new form to display our gallery for editing.
        $form = new Varien_Data_Form(
        	array(
            	'id' => 'edit_form',
            	'action' => $this->getUrl(
            		'indradesign_imagegallery_admin/gallery/edit',
                array(
                    '_current' => true,
                    'continue' => 0,
                )
            ),
            'method' => 'post',
            'enctype' => 'multipart/form-data',
        ));
        $form->setUseContainer(true);
        $this->setForm($form);

        // Define a new fieldset. We need only one for our simple entity.
        $fieldset = $form->addFieldset(
            'general',
            array(
                'legend' => $this->__('Gallery Image Details')
            )
        );

        $gallerySingleton = Mage::getSingleton(
            'indradesign_imagegallery/gallery'
        );

        // Add the fields that we want to be editable.
        $this->_addFieldsToFieldset($fieldset, array(
            'url_key' => array(
                'label' => $this->__('URL Key'),
                'input' => 'text',
                'required' => true,
            ),
            'description' => array(
                'label' => $this->__('Description'),
                'input' => 'textarea',
                'required' => true,
            ),
			'image_1'=> array(
                'label' => $this->__('Image'),
                'input' => 'file',
                'required' => true,
			),
        ));

        return $this;
    }

    protected function _addFieldsToFieldset(Varien_Data_Form_Element_Fieldset $fieldset, $fields)
    {
        $requestData = new Varien_Object($this->getRequest()->getPost('imageData'));

        foreach ($fields as $name => $_data) 
        {
            if($requestValue = $requestData->getData($name)) {
                $_data['value'] = $requestValue;
            }

            // Wrap all fields with imageData group.
            $_data['name'] = "imageData[$name]";

            // Generally, label and title are always the same.
            $_data['title'] = $_data['label'];

            // If no new value exists, use the existing gallery data.
            if (!array_key_exists('value', $_data)) {
                $_data['value'] = $this->_getGalleryImage()->getData($name);
            }

            // Finally, call vanilla functionality to add field.
            $fieldset->addField($name, $_data['input'], $_data);
        }
        return $this;
    }

    protected function _getGalleryImage()
    {
        if(!$this->hasData('gallery')) 
        {
            // This will have been set in the controller.
            $gallery = Mage::registry('current_gallery');

            // Just in case the controller does not register the gallery.
            if (!$gallery instanceof IndraDesign_ImageGallery_Model_Gallery) 
    		{
                $gallery = Mage::getModel('indradesign_imagegallery/gallery');
            }

            $this->setData('gallery', $gallery);
        }

        return $this->getData('gallery');
    }
}