<?php
class IndraDesign_ImageGallery_Block_Adminhtml_Gallery extends Mage_Adminhtml_Block_Widget_Grid_Container
{
    protected function _construct()
    {
        parent::_construct();

        /**
         * The $_blockGroup property tells Magento which alias to use to
         * locate the blocks to be displayed in this grid container.
         * In our example, this corresponds to ImageGallery/Block/Adminhtml.
         */
        $this->_blockGroup = 'indradesign_imagegallery_adminhtml';

        /**
         * $_controller is a slightly confusing name for this property.
         * This value, in fact, refers to the folder containing our
         * Grid.php and Edit.php - in our example,
         * ImageGallery/Block/Adminhtml/Gallery. So, we'll use 'gallery'.
         */
        $this->_controller = 'gallery';

        /**
         * The title of the page in the admin panel.
         */
        $this->_headerText = Mage::helper('indradesign_imagegallery')->__('Image Gallery');
    }

    public function getCreateUrl()
    {
        /**
         * When the "Add" button is clicked, this is where the user should
         * be redirected to - in our example, the method editAction of
         * GalleryController.php in ImageGallery module.
         */
        return $this->getUrl('indradesign_imagegallery_admin/gallery/edit');
    }
}