<?php
class IndraDesign_ImageGallery_Adminhtml_GalleryController
    extends Mage_Adminhtml_Controller_Action
{
    /**
     * Instantiate our grid container block and add to the page content.
     * When accessing this admin index page, we will see a grid of all
     * galleries currently available in our Magento instance, along with
     * a button to add a new one if we wish.
     */
    public function indexAction()
    {
        // instantiate the grid container
        $galleryBlock = $this->getLayout()->createBlock('indradesign_imagegallery_adminhtml/gallery');

        // Add the grid container as the only item on this page
        $this->loadLayout()->_addContent($galleryBlock)->renderLayout();
    }
    
    /**
     * This action handles both viewing and editing existing galleries.
     */
    public function editAction()
    {
        /**
         * Retrieve existing gallery data if an ID was specified.
         * If not, we will have an empty gallery entity ready to be populated.
         */
        $gallery = Mage::getModel('indradesign_imagegallery/gallery');
        if ($galleryId = $this->getRequest()->getParam('id', false)) 
        {
            $gallery->load($galleryId);

            if (!$gallery->getId())
            {
            	$this->_getSession()->addError($this->__('This gallery image no longer exists.'));                
                return $this->_redirect('indradesign_imagegallery_admin/gallery/index');
            }
        }

        // process $_POST data if the form was submitted
        if ($postData = $this->getRequest()->getPost('imageData')) 
        {
        	if(isset($_FILES['imageData']['name']['image_1']) and (file_exists($_FILES['imageData']['tmp_name']['image_1']))) 
        	{
	            try 
	            {
					$me = array('tmp_name'=>$_FILES['imageData']['tmp_name']['image_1'], 'name'=>$_FILES['imageData']['name']['image_1']);
					$uploader = new Varien_File_Uploader($me);
					
					$uploader->setAllowedExtensions(array('jpg','jpeg','gif','png'));
					$uploader->setAllowRenameFiles(false);
					$uploader->setFilesDispersion(false);	
					$path = Mage::getBaseDir('media') . DS . 'dhl' . DS;
					$resultNow = $uploader->save($path, $_FILES['imageData']['name']['image_1']);
	                $gallery->addData($postData);
	                if(isset($resultNow['path']) &&  isset($resultNow['file']))
	                {
	                	$gallery->setLocation($resultNow['file']);
	               	}
	                $gallery->save();
	                $this->_getSession()->addSuccess($this->__('The gallery has been saved.'));
	
	                // redirect to remove $_POST data from the request
					return $this->_redirect('indradesign_imagegallery_admin/gallery/edit', array('id' => $gallery->getId()));

	            }
	            catch (Exception $e) 
	            {
	                Mage::logException($e);
	                $this->_getSession()->addError($e->getMessage());
	            }

            /**
             * If we get to here, then something went wrong. Continue to
             * render the page as before, the difference this time being
             * that the submitted $_POST data is available.
             */
             }
        }

        // Make the current gallery object available to blocks.
        Mage::register('current_gallery', $gallery);

        // Instantiate the form container.
        $galleryEditBlock = $this->getLayout()->createBlock('indradesign_imagegallery_adminhtml/gallery_edit');

        // Add the form container as the only item on this page.
        $this->loadLayout()
            ->_addContent($galleryEditBlock)
            ->renderLayout();
    }

    public function deleteAction()
    {
        $gallery = Mage::getModel('indradesign_imagegallery/gallery');

        if ($galleryId = $this->getRequest()->getParam('id', false)) {
            $gallery->load($galleryId);
        }

        if (!$gallery->getId())
        { 
        	$this->_getSession()->addError($this->__('This gallery no longer exists.'));
            return $this->_redirect('indradesign_imagegallery_admin/gallery/index');
        }

        try
        {
            $gallery->delete();
            $this->_getSession()->addSuccess($this->__('The gallery image has been deleted.'));
        } 
        catch (Exception $e) 
        {
            Mage::logException($e);
            $this->_getSession()->addError($e->getMessage());
        }

        return $this->_redirect('indradesign_imagegallery_admin/gallery/index');
    }

    /**
     * Without
     * this method the ACL rules configured in adminhtml.xml are ignored.
     */
    protected function _isAllowed()
    {
        /**
         * we include this switch to demonstrate that you can add action
         * level restrictions in your ACL rules. The isAllowed() method will
         * use the ACL rule we have configured in our adminhtml.xml file:
         * - acl
         * - - resources
         * - - - admin
         * - - - - children
         * - - - - - smashingmagazine_branddirectory
         * - - - - - - children
         * - - - - - - - brand
         *
         * eg. you could add more rules inside brand for edit and delete.
         */
        
        $actionName = $this->getRequest()->getActionName();
        
        switch ($actionName) 
        {
            case 'index':
            case 'edit':
            case 'delete':
            default:
                $adminSession = Mage::getSingleton('admin/session');
                $isAllowed = $adminSession->isAllowed('indradesign_imagegallery/gallery');
                break;
        }

        return $isAllowed;
    }
}